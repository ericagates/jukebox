package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class DustinLynch {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public DustinLynch() {
    }
    
    public ArrayList<Song> getDustinLynchSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Cowboys and Angels", "Dustin Lynch");             //Create a song
         Song track2 = new Song("Good Girl", "Dustin Lynch");         //Create another song
         Song track3 = new Song("Small Town Boy", "Dustin Lynch");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for "Dustin Lynch"
         this.albumTracks.add(track2); 											//Add the second song to song list for "Dustin Lynch" 
         this.albumTracks.add(track3); 											//Add the third song to song list for "Dustin Lynch"s 
         return albumTracks;                                                    //Return the songs for "Dustin Lynch" in the form of an ArrayList
    }
}