package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class AlexiMurdoch {

	ArrayList<Song> albumTracks;
    String albumTitle;

    public AlexiMurdoch() {
    }

    public ArrayList<Song> getAlexiMurdochSongs() {

    	albumTracks = new ArrayList<Song>();                                //Instantiate the album so we can populate it below
    	Song track1 = new Song("All My Days", "Alexi Murdoch");             //Create a song
    	Song track2 = new Song("Orange Sky", "Alexi Murdoch");       		//Create another song
    	Song track3 = new Song("Slow Revolution", "Alexi Murdoch");			//Create another song
    	this.albumTracks.add(track1);                                       //Add the first song to song list for Alexi Murdoch
        this.albumTracks.add(track2);                                       //Add the second song to song list for Alexi Murdoch
        this.albumTracks.add(track3);										//Add the third song to the song list for Alexi Murdoch
        return albumTracks;                                                 //Return the songs for Alexi Murdoch in the form of an ArrayList
    }
}
