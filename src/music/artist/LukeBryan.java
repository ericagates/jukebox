package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class LukeBryan {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public LukeBryan() {
    }
    
    public ArrayList<Song> getLukeBryanSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Play It Again", "Luke Bryan");         //Create a song
         Song track2 = new Song("Turnip Greens", "Luke Bryan");         //Create another song
         Song track3 = new Song("Crash My Party", "Luke Bryan");        //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Luke Bryan
         this.albumTracks.add(track2); 											//Add the second song to song list for Luke Bryan
         this.albumTracks.add(track3); 											//Add the third song to song list for Luke Bryan 
         return albumTracks;                                                    //Return the songs for Luke Bryan in the form of an ArrayList
    }
}